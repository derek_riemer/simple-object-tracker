# Build customizations
# Change this file instead of sconstruct or manifest files, whenever possible.

# Full getext (please don't change)
_ = lambda x : x

# Add-on information variables
addon_info = {
	# add-on Name
	"addon-name" : "ObjectTracker",
	# Add-on description
	# TRANSLATORS: Summary for this add-on to be shown on installation and add-on information.
	"addon-summary" : _("Background Object Tracker is an addon that allows tracking of objects that are purposely not spoken."),
	# Add-on description
	# Translators: Long description to be shown for this add-on on installation and add-on information
	"addon-description" : _("""This addon speaks text on objects that purposely are not spoken. Examples of this are in ITunes, where the progress bar if spoken would lead to random talkking. Use object navigation (see the users guide for help with this) to locate the text you want NVDA to speak. Then while the navigator object is still on the object you wish to track press 'nvda+shift+g' to track the object. NVDA will announce any text as it is updated. A quick example, while itunes is open use object navigation. Navigate to the top level window. Then navigate into that window and move through the objects going right until you hear nvda say something like lcd section window. Navigate into this until you hear nvda say window. At this point move right until you hear the name of the currently playing song. Go into that window and you will hear the name of that song again. Press nvda+shift+g to have nvda start tracking the object. Press the right arrow key to move to the next song when this is done and you should hear nvda say the title of the song. Another great use of this feature is for speech recognition window on windows 7. Please email the support digest and I will explain how to do this."""),
	# version
	"addon-version" : "0.5.1",
	# Author(s)
	"addon-author" : "Derek Riemer <driemer.riemer@gmail.com>",
	# URL for the add-on documentation support
	"addon-url" : None
}


import os.path

# Define the python files that are the sources of your add-on.
# You can use glob expressions here, they will be expanded.
pythonSources = ["addon\globalPlugins\object-tracker.py"]

# Files that contain strings for translation. Usually your python sources
i18nSources = pythonSources + ["buildVars.py"]

# Files that will be ignored when building the nvda-addon file
# Paths are relative to the addon directory, not to the root directory of your addon sources.
excludedFiles = []
