import languageHandler
import addonHandler
import globalPluginHandler
import api
import ui
from logHandler import log
addonHandler.initTranslation()
obj_tracking=None
followname=None
def setObject():
	global followname, obj_tracking
	if obj_tracking.value!=None:
		valueObj=len(obj_tracking.value)
	else:
		valueObj=0
	if obj_tracking.name!=None:
		log.debug("yes, my name is %s"%obj_tracking.name)
		nameObj=len(Obj_tracking.name)
	else:
		nameObj=0
	if valueObj>=nameObj:
		followname = str(obj_tracking.name)
	else:
		followname=str(obj_tracking.value)


class GlobalPlugin(globalPluginHandler.GlobalPlugin):
	def script_navigateToObj(self, gesture):
		try:
			api.setNavigatorObject(obj_tracking)
			#translators: the message spoken that causes nvda to prompt that the navigator object is the tracked object.
			ui.message(_("The tracked object has been set at the nvda navigator object."))
			log.debug("The object {} was set as the users navigator object.".format(obj_tracking))
		except:
			log.debug("error")
			#translators: the message spoken when object cannot be followed.
			ui.message(_("Error following object."))
	def script_setObject(self, gesture):
		global obj_tracking, followname
		#import our globals
		if obj_tracking==None:
			#If the addon is not following anything then we should follow the navigator object.
			obj_tracking=api.getNavigatorObject()
			#translators: The message spoken to inform the user object has been followed.
			ui.message(_("the object named {} is being tracked. NVDA will say any text as it is updated.".format(obj_tracking.name)))
			setObject()
		else:
			#translators: The message instructing that the object isn't being followed any more.
			ui.message(_("object {} is not being followed anymore.".format(obj_tracking.name)))
			followname=None
			obj_tracking=None
	def script_speakNavigation(self, gesture):
		global obj_tracking, followname
		try:
			ui.message(followname)
		except NameError, AttributeError:
			pass
	def event_valueChange(self, obj, nextHandler):
		global obj_tracking
		try:
			if obj_tracking==obj:
				#the object  being tracked has changed values. 
				obj_tracking=obj
				setObject()
				#we want to follow this new instance of the updated object in obj_tracking not the old object whose value has since changed.
				ui.message(obj.value)
		except AttributeError:
			#this object seems to raise an atribute error when the value changes to None or from None, but we really don't want to speak objects whose values change from none anyway because when obj_tracking is None it isn't being followed.
			pass
		nextHandler()
	def event_nameChange(self, obj, nextHandler):
		global obj_tracking
		try:
			if obj_tracking==obj :
				#the object is still the same so this object indeed is the same object.
				ui.message(obj.name)
				obj_tracking=obj
				setObject()
		except AttributeError, NameError:
			pass
		nextHandler()
	__gestures={
		"kb:NVDA+shift+g": "setObject",
		"kb:NVDA+g": "speakNavigation",
		"kb:NVDA+o": "navigateToObj",
	}